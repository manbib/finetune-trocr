# Fine-tuning TrOCR for Optical Character Recognition

## Overview
This repository hosts a Jupyter notebook dedicated to fine-tuning the TrOCR model for advanced optical character recognition tasks. TrOCR, introduced by Minghao Li et al. in their 2021 paper, leverages the power of Transformer-based models for OCR by combining an image Transformer as encoder and a text Transformer as decoder. This notebook explores the model's capabilities and demonstrates how to fine-tune it on the im2latex100k and im2latex230k datasets.

## Model Description
The TrOCR model is a state-of-the-art encoder-decoder framework, utilizing an image Transformer (initialized from BEiT weights) and a text Transformer (initialized from RoBERTa weights) for OCR tasks. It's designed to process images as sequences of fixed-size patches, effectively translating visual data into textual outputs.

## Fine-tuned Model
The model fine-tuned as part of this project is available on Hugging Face at [ManBib/trocr-large-printed-im2latex](https://huggingface.co/ManBib/trocr-large-printed-im2latex). This version is specifically optimized for recognizing mathematical expressions and formulas in the im2latex100k and im2latex230k datasets.

## Usage
Open the `finetune_trocr_im2latex.ipynb` notebook in a Jupyter environment to start. The notebook guides you through the process of loading the TrOCR model, preparing your dataset, fine-tuning the model, and evaluating its performance on OCR tasks. Step-by-step instructions within the notebook provide detailed guidance on how to proceed.


## Citation

```
@misc{li2021trocr,
      title={TrOCR: Transformer-based Optical Character Recognition with Pre-trained Models}, 
      author={Minghao Li and Tengchao Lv and Lei Cui and Yijuan Lu and Dinei Florencio and Cha Zhang and Zhoujun Li and Furu Wei},
      year={2021},
      eprint={2109.10282},
      archivePrefix={arXiv},
      primaryClass={cs.CL}
}
```
